
import java.util.*;

/*
 * https://www.hackerrank.com/contests/uap-si-e/challenges/berapa-laman
 */

public class BerapaLaman {
static Scanner sc = new Scanner(System.in);
public static void main(String[] args) {
    int lmn = sc.nextInt(); 
    int aktif = sc.nextInt();
    int r = sc.nextInt();
    int min = aktif - r / 2;
    int max = aktif + r / 2;
    
    if (min < 1) {
        min = 1;
    }
    if (aktif - 1 >= 1){
        System.out.print("Prev ");
    }
    if(min >= 1 && max <= lmn){
        for(int i = 0; i <= r; i++){
            System.out.print(min + " ");
            min++;
        }
    }else if(max > lmn){
        max= lmn - r;
        for(int i = 0; i <= r;i++){
            System.out.print(max + " ");
            max++;
        }
    }
    if(aktif + 1 <= lmn){
        System.out.print("Next");
    }
}
}
