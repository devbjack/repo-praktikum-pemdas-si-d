package Tugas;

import java.util.*;

/*
 * https://www.hackerrank.com/contests/tugas-terakhir-si-e/challenges/pilih-bola
 */

public class PilihBola {

    static Scanner sc = new Scanner(System.in);
    static int faktorial(int a){
        if(a <= 1){
            return 1;
        }else{
            return a*faktorial(a-1);
        }
        
    }
    static int oddBall(int n, int nAndi, int nBudi, String nama){
        switch (nama){
            case "Andi":
                return faktorial(n)/(faktorial(n-nAndi)*faktorial(nAndi));
            case "Budi":
                return faktorial(n)/(faktorial(n-nBudi)*faktorial(nBudi));
        }
        return 0;
    }

    /*
     *DILARANG MENGEDIT DI BAWAH INI!!1
     */

    public static void main(String[] args) {
        /* n = a, nm = b,nb=c*/
        int n = sc.nextInt();
        int nm = sc.nextInt();
        int nb = sc.nextInt();
        sc.nextLine();
        String nama = sc.nextLine();

        System.out.println(oddBall(n,nm,nb,nama));


    }
}
