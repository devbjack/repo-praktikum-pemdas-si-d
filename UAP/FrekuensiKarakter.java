import java.util.*;

/*
 * https://www.hackerrank.com/contests/uap-si-e/challenges/frekuensi-karakter-1
 */

public class FrekuensiKarakter {

    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        char[] karakter = new char[127];
        int[] counter = new int [127];
        
        for (int i = 0; i < karakter.length; i++) {
            karakter[i] = (char)(i);
        }

        String input = sc.nextLine();
        char[] inputArray = input.toCharArray();

        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < karakter.length; j++) {
                if(inputArray[i]== karakter[j]){
                    counter[j]++;
                }
            }
        }

        for (int i = 0; i < karakter.length; i++) {
            if(counter[i] != 0){
                System.out.println(karakter[i] + ": " + counter[i]);
            }
        }

    }
}
