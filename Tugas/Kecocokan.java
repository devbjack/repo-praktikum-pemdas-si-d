package Tugas;
import java.util.*;

/*
 * https://www.hackerrank.com/contests/tugas-terakhir-si-e/challenges/kecocokan-yang-aneh
 */

public class Kecocokan {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.next();
        System.out.println(bayiCheck(input));
        sc.close();
    }
    static String bayiCheck(String s){
        int counter = s.length();
        char[] karakter = s.toCharArray();
        Arrays.sort(karakter);
        for (int i = 0; i < karakter.length-1; i++) {
            if(karakter[i]==karakter[i+1])--counter;
        }

        if(counter%2==0){
            return "BAGUS";
        }else{
            return "KURANG";
            }    
        
    }
}

