package Tugas;
import java.util.Scanner;

/*
 * https://www.hackerrank.com/contests/tugas-6-si-e/challenges/kalkulator-sederhana-3
 */

public class Recursive {
    static void rahasia(int n){
        if(n==0){
            return;
        }
        if (n<=1){
            System.out.println("1 ganjil");
            return;
        }else{
            System.out.print(n + " ");
                if(n%2==0){
                    System.out.println("genap");
                }else{
                    System.out.println("ganjil");
                }
        }
        rahasia(--n);
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        rahasia(sc.nextInt());
        sc.close();
    }
    
}