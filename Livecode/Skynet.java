package Livecode;

import java.util.*;

/*
 * https://www.hackerrank.com/contests/livecode-6-si-e/challenges/skynet-v0-5
 */

public class Skynet {
    static Scanner sc = new Scanner(System.in);

  static String posisiRobot(int n,String[] perintah, int p, int l){
        int x = 0; int y = 0;
        for(int i = 0; i < perintah.length; i++){
            switch(perintah[i]){
                case "up":
                    ++x;
                    break;
                case "right":
                    ++y;
                    break;
                case "down":
                    --x;
                    break;
                case "left":
                    --y;
                    break;
            }
        }
        if(x>=0 && x<p && y>=0 && y<l){
            return String.format("(%d,%d)", y,x);
        }else{
            return "Di luar peta";
        }
    }

    /*
     *DILARANG MENGEDIT DI BAWAH INI
     */
    public static void main(String[] args) {
        int nMoves = sc.nextInt();
        int panjang = sc.nextInt(),lebar = sc.nextInt();
        String[] instructions = new String[nMoves];

        for(int i=0;i<nMoves;i++){
            instructions[i] = sc.next();
        }
        System.out.println(posisiRobot(nMoves, instructions, panjang, lebar));

    }
}

