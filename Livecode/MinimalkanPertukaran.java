package Livecode;

import java.util.*;

/*
 * https://www.hackerrank.com/contests/livecode-terakhir-si-e/challenges/minimalkan-pertukaran/copy-from/1339129628
 */

public class MinimalkanPertukaran {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        String input = sc.nextLine();
        String[] split = input.split(" ");
        int kanan = 0, kiri = 0, cara = 0;

        //menghitung cara
        for (int i = 0; i < split.length-1; i++) {
            //kalo udah sepasang <--> langsung lewati
            if(split[i].equals("<-")&&split[i+1].equals("->")){
                ++i;
                
            }
            //dihitung sebagai cara perpindahan jika muncul -> terlebih dahulu daripada <-
            else if(split[i].equals("<-")){
                ++kiri;
                
            }else if(split[i].equals("->")){
                ++kanan;
                if(kanan>kiri){
                    ++cara;
                    --kanan;
                }
            }
        }
        System.out.println(cara);
    }
}
