package UTP;

import java.util.Scanner;

import javax.print.DocFlavor.STRING;

public class Persiapan {
    public static void main(String[] args) {
        //Tipe-tipe data, variabel, kontanta
        // short b; decimal paling kecil
        // int a; decimal tengah-tengah
        // long c; decimal besar

        // float d; floating point kecil
        // double e; floating point lebih besar

        // char f; 1 karakter saja dengan '' (petik satu)
        // String g; semua karakter didalam "" (petik dua)

        // final int y = 10; konstanta -> tidak bisa berubah

        // boolean x; menyimpan nilai logika, true or false (false = 0, true = 1)

        //input dan output
        // Scanner sc = new Scanner(System.in);
        // System.out.print("Hello World!\n"); tanpa new line (enter)
        // System.out.println("Hello World!" + "2"); dengan new line
        
        
        //format output
        // System.out.printf("Hello World%6c %d %s %6.2f",'!', 2, "Budi", 2.5); 
        /* catatan format print
         * %c -> char
         * %s -> String
         * %d -> decimal (angka 10, tanpa koma, int/short/long)
         * %f -> floating point (angka koma, float/double)
         * 
         * %6.2f -> berada setelah 6 spasi, 2 angka dibelakang koma
         * sisanya bisa dicoba coba sendiri, perlu banyak latihan
         */

        //operator matematika dan logika
            /*
                operator matematika
             * (+ - / * %)
                
                operator logika
                (< <= == => >)

                // boolean x = 6 == 5;
                // System.out.println(x);
                
                // int a = 2;
                */
               /*
                * tambahan operator logika
                * bisa tambahkan '&&' -> and
                * dan '||' -> or
                * and dan or untuk menggabungkan beberapa operasi logika
                * 
                * contoh :
                * (a>5) && (a%2 == 0) -> artinya a lebih besar dari 5 dan a adalah angka genap
                */
            
        //math method
        // System.out.println(Math.min(7,5)); nilai paling kecil
        // System.out.println(Math.max(3, 4)); nilai paling besar
        // System.err.println(Math.abs(-9)); nilai mutlak
        // System.out.println(Math.sqrt(16)); nilai akar
        // System.out.println(Math.pow(3, 2)); nilai pangkat

        /*
         * method math untuk pembulatan
         * Math.round() -> membulatkan sesuai ketentuan
         * Math.ceil() -> pembulatan keatas
         * Math.floor() -> pembulatan kebawah
         */

        //string method
        // String nama = "BonA";
        // String nama1 = "BonAAA";
        // System.out.println(nama1.equals(nama1));
        /*
         * [variabel string1].equals([variabel string2]) -> untuk membandingkan 2 string
         * [variabel string1].equalsIgnoreCase([variabel string2]) -> untuk membandingkan 2 string tanpa memerhatikan case
         * [variabel string1].contains([variabel string2]) -> untuk mengecek apakah string1 mengandung string 2
         */
        
        //proses seleksi

        /* form standar
         * if (condition 1){        --> harus diawali dengan if
         *  statement 1;            
         * }else if(condition 2){   --> ga harus ada else if
         * statement 2;
         * }else{                   --> ga harus ada else
         *  default;
         * }
         */
        //int a = 13, b= 7;
        // if (a<b){
        //     System.out.println("a lebih kecil");
 
        // }
        // else if (b<a){
        //     System.out.println("b lebih kecil");
    
        // }
        // else {
        //     System.out.println("sama dengan");
        // }
        // if (a>b){
        //     if (a%2==0){
        //         System.out.println("A adalah Angka Genap");
        //     }
        //         else {
        //         System.out.println("A adalah Angka Ganjil");
        //     }
        //     }
        // else if (a<b){
        //     if (b%2==0){
        //         System.out.println("B adalah Angka Genap");
        //     }
        //         else {
        //         System.out.println("B adalah Angka Ganjil");
        //     }
        //  }
        // else {
        //     System.out.println("Nilai sama");
        // }
        String nama = "BOna";
        switch (nama.toLowerCase()) {               // dimulai dengan yang ingin dicek
            case "bona" :                           // case adalah kondisi pengecekan
                System.out.println("nama bona");  
                break;
            case "royyan" :
                System.out.println("nama royyan");
                break;
            default:
                System.out.println("nama tidak dikenali");
                break;
        }
        
        // switch  (a+b){
        //     case :
        //         switch (key) {
        //             case value:
                        
        //                 break;
                
        //             default:
        //                 break;
        //         }
        //         break;
        
        //     default:
        //         break;
        // } 
    }
}
