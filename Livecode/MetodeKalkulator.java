package Livecode;

import java.util.Scanner;

/*
 * https://www.hackerrank.com/contests/livecode-6-si-e/challenges/metode-kalkulator
 */

public class MetodeKalkulator {
    static Scanner read= new Scanner(System.in);
    static void penjumlahan(int a,int b){
        System.out.println("=== METHOD PENJUMLAHAN ===");
        int hasil = a + b;
        System.out.println(a + " + " + b + " = " + hasil);
    }
    static void pengurangan(int a,int b){
        System.out.println("=== METHOD PENGURANGAN ===");
        int hasil = a - b;
        System.out.println(a + " - " + b + " = " + hasil);
    }
    static void pembagian(int a,int b){
        System.out.println("=== METHOD PEMBAGIAN ===");
        int hasil = a / b;
        System.out.println(a + " / " + b + " = " + hasil);
    }
    static void perkalian(int a,int b){
        System.out.println("=== METHOD PERKALIAN ===");
        int hasil = a * b;
        System.out.println(a + " x " + b + " = " + hasil);
    }
    public static void main(String[] args) {
        
        String x= read.nextLine();
        String [] sSplit=x.split(" ");
        
        switch (sSplit[0]) {
            case "+":
				penjumlahan(Integer.valueOf(sSplit[1]), Integer.valueOf(sSplit[2]));
                break;
            case "-":
                pengurangan(Integer.valueOf(sSplit[1]), Integer.valueOf(sSplit[2]));
                break;
            case "*":
                perkalian(Integer.valueOf(sSplit[1]), Integer.valueOf(sSplit[2]));
                break;
            case "/":
                pembagian(Integer.valueOf(sSplit[1]), Integer.valueOf(sSplit[2]));
                break;
        
            default:
				//
                break;
        }
    }
}