package Livecode;

import java.util.*;

/*
 * https://www.hackerrank.com/contests/livecode-terakhir-si-e/challenges/skynet-v1-0
 */

public class SkynetV1 {
    static Scanner sc = new Scanner(System.in);

    static char[][] kemanaSiRobot(int n,String[] perintah, int a, int b){
        if(a<=0 && a<=0){
            return null;
        }
        int x = 0; int y = 0;
        char[][] map = new char[a][b];
        for(int i = 0; i < n; i++){
            switch (perintah[i]){
                case "up":
                    --x;
                    break;
                case "down":
                    ++x;
                    break;
                case "left":
                    --y;
                    break;
                case "right":
                    ++y;
                    break;
            }   
        }for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (i==x&&j==y){
                    map[i][j]='H';
                }else{
                    map[i][j]='O';
                }
            }
            
        }
        return map;
    }

    /*
     *DILARANG MENGEDIT DI BAWAH INI
     */
    public static void main(String[] args) {
            //System.out.println("inputs n , a, b,and instructions");
            int nMoves = sc.nextInt();
            int n = sc.nextInt(),m = sc.nextInt();

            String[] instructions = new String[nMoves];
            char[][] map = new char[n][m];

            //System.out.println("input instructions");
            sc.nextLine(); /* good measures */
            for(int i=0;i<nMoves;i++){
                instructions[i] = sc.nextLine();
            }

            map = kemanaSiRobot(nMoves,instructions,n,m);

            

            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    System.out.print(map[i][j]);
                }
                System.out.println();
            }

 
    }
}
